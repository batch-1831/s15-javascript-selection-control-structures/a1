// test for script attachment to htmk
// console.log("Hello World!");

// #3 and #4
let inputNum1 = parseInt(prompt("Provide a number: "));
let inputNum2 = parseInt(prompt("Provide another number: "));

let sum = inputNum1 + inputNum2;
let output;

if (sum < 10) {
    output = inputNum2 + inputNum1;
    console.warn("The total of two numbers is less than " + output);
}
else if (sum >= 10 && sum <= 20) {
    output = inputNum2 - inputNum1;
    alert("The difference of two numbers are: " + output);
}
else if (sum >= 21 && sum <= 29) {
    output = inputNum2 * inputNum1;
    alert("The product of two numbers are: " + output);
}
else {
    output = inputNum2 / inputNum1;
    alert("The qoutient of two numbers are: " + output);
}

// #5 
let inputName = prompt("What is your name? ");
let inputAge = prompt("What is your age? ");

if (inputName == "" || inputAge == "") {
    alert("Are you a time traveler?");
}
else if (inputName == "" && inputAge == "") {
    alert("Are you a time traveler?");
}
else {
    alert("Hello " + inputName + ". Your age is " + inputAge);
}

// #6
isLegalAge(inputAge);

function isLegalAge(age) {
    if (parseInt(age) >= 18) {
        alert("You are of legal age.");
    }
    else {
        alert("You are not allowed here.");
    }
}

// #7
switch (parseInt(inputAge)) {
    case 18:
        alert("You are now allowed to party.");
        break;
    case 21:
        alert("You are now part of adult society.");
        break;
    case 65:
        alert("We thank you for your contribution to society.");
        break;
    default:
        alert("Are you sure you're not an alien?");
        break;
}

// #8
try {
    isLegalAg(age);
}
catch (err) {
    console.warn(err.message);
}
finally {
    console.log("Error detected!");
}